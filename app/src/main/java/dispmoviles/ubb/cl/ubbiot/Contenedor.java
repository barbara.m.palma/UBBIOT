package dispmoviles.ubb.cl.ubbiot;


//Clase contenedora de los datos recibidos desde el web service
//Contiene los 3 datos que devuelve cada medida: fecha, hora y valor
public class Contenedor {
    private String fecha;
    private String hora;
    private double valor;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}

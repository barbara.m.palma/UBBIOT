package dispmoviles.ubb.cl.ubbiot;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

//Clase necesaria para implementar el datepicker
public class DatePickerFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //crea el nuevo datepicker y le entrega la fecha en que se iniciara 
        DatePickerDialog d = new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), 2018, 04, 26);
        return d;
    }
}

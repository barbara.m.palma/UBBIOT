package dispmoviles.ubb.cl.ubbiot;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    //Declaracion de variables necesarias
    private ArrayList<Contenedor> temperaturas;
    private TextView minimo, maximo, promedio;
    private double tempMax, tempMin, tempProm, temperatura;
    private Button botonFecha;
    private String f, fechaSeleccionada;
    private RelativeLayout layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se buscan los view por su id y se inicializan las variables para su uso
        minimo = findViewById(R.id.minimo);
        maximo = findViewById(R.id.maximo);
        promedio = findViewById(R.id.promedio);
        botonFecha = findViewById(R.id.fecha);
        layout = findViewById(R.id.relativeLayout);

        //se crea el arraylist
        temperaturas = new ArrayList<>();

        //Al hacer click en el boton para seleccionar fecha, se cambiara su color de fondo
        //Y tambien se abrira el datepicker
        botonFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.support.v4.app.DialogFragment d = new DatePickerFragment();
                d.show(getSupportFragmentManager(), "Date picker");

                Random random = new Random();
                int color= Color.argb(255, random.nextInt(256),random.nextInt(256),random.nextInt(256));
                botonFecha.setBackgroundColor(color);
            }
        });

    }

    //funcion que obtiene la fecha seleccionada en el datepicker
    //tambien llama a la funcion que captura los datos del dia seleccionado
    @Override
    public void onDateSet(DatePicker datePicker, int anio, int mes, int dia) {
        String m, d, a;
        mes++;
        m = Integer.toString(mes);
        a = Integer.toString(anio);
        d = Integer.toString(dia);
        if(mes<10){
            m = "0" + m;
        }
        if (dia<10){
            d = "0" + d;
        }
        f=d+m+a;
        fechaSeleccionada= d+"/"+m+"/"+a;
        capturarDatos();
    }

    //funcion que captura los datos desde el web service
    private void capturarDatos() {
        temperaturas.clear();
        Log.d("LOG", "se llamó");
        //se establece la url, con el dia y sensor a consultar
        String WS_URL = "http://arrau.chillan.ubiobio.cl:8075/ubbiot/web/mediciones/medicionespordia/09bXdtHIwJ/E1yGxKAcrg/"+f;
        Log.d("LOG", "string: " + WS_URL);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(
                Request.Method.GET,
                WS_URL,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respuesta = new JSONObject(response);
                            JSONArray responseJson = respuesta.getJSONArray("data");

                            //recorre el arreglo entregado por el web service y convierte los
                            // datos recibidos en objetos tipo contenedor
                            for(int i = 0; i < responseJson.length(); i++){
                                JSONObject o = responseJson.getJSONObject(i);
                                Contenedor cont = new Contenedor();

                                cont.setFecha(o.getString("fecha"));
                                cont.setHora(o.getString("hora"));
                                cont.setValor(o.getInt("valor"));

                                temperaturas.add(cont);

                            }

                            Log.d("LOG", "cantidad: " + temperaturas.size());
                            //llama a la funcion que calcula los datos a mostrar al usuario
                            calculaDatos();
                        } catch (JSONException e) {
                            //si el dia consultado no posee datos, se generara un toast para informar
                            // de la situacion al usuario, cambia el fondo y el contenido de los text view
                            e.printStackTrace();
                            generateToast("No existen datos para el día consultado");
                            minimo.setText("-T°");
                            maximo.setText("-T°");
                            promedio.setText("-T°");
                            layout.setBackgroundResource(R.drawable.bad);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("LOG WS", error.toString());
                generateToast("Error en el WEB Service");
            }
        }
        );
        requestQueue.add(request);
    }

    //funcion que calcula la temperatura maxima, minima y promedio de un dia
    private void calculaDatos() {
        int size = temperaturas.size();
        tempMax = Integer.MIN_VALUE;
        tempMin = Integer.MAX_VALUE;
        tempProm = 0;
        //Recorre el arraylist de las temperaturas del dia consultado
        for (int i = 0; i < temperaturas.size()-1; i++) {
            temperatura = temperaturas.get(i).getValor();
            if (temperatura<tempMin){
                tempMin=temperatura;
            }
            if (temperatura>tempMax){
                tempMax=temperatura;
            }
            tempProm=tempProm+temperatura;
        }
        //calcula el promedio, muestra los datos calculados en el textview
        //y cambia el fondo del layout
        tempProm= tempProm/size;
        minimo.setText(Double.toString(tempMin)+"°C");
        maximo.setText(Double.toString(tempMax)+"°C");
        promedio.setText(Double.toString((int)tempProm)+"°C");
        botonFecha.setText("Estas consultando el dia " + fechaSeleccionada);
        cambiarFondo(tempProm);
    }

    //funcion que cambia el fondo del layout dependiendo de la temperatura promedio del dia consultado
    private void cambiarFondo(double promedioTemperatura){
        if(promedioTemperatura< 5){
            layout.setBackgroundResource(R.drawable.jon);
        }else if(promedioTemperatura >=5 && promedioTemperatura<8){
            layout.setBackgroundResource(R.drawable.sith);
        }else if(promedioTemperatura >=8 && promedioTemperatura<15){
            layout.setBackgroundResource(R.drawable.abrigado);
        }else if(promedioTemperatura >=15 && promedioTemperatura<20){
            layout.setBackgroundResource(R.drawable.simpsons2);
        }else if(promedioTemperatura >=20 && promedioTemperatura<30) {
            layout.setBackgroundResource(R.drawable.simpsons);
        }else if(promedioTemperatura >=30) {
            layout.setBackgroundResource(R.drawable.dog);
        }
    }

    //funcion que genera un toast
    private void generateToast(String msg){
        Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_SHORT).show();
    }

}
